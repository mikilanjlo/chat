﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessagePack
{

    [CreateAssetMenu(fileName = "Name Companion", menuName = "Companion")]
    public class Companion : ScriptableObject
    {
        public string Name;
    }
}