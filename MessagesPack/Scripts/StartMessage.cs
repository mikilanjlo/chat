﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace MessagePack
{
    public class StartMessage : Node, IMessageNodes
    {
        [Output] public EmptyPort Start;

        public bool HasMessage => false;

        protected override void Init()
        {
            OnEnter();
        }

        public bool MoveNext(int variantInd = 0)
        {
            NodePort exitPort = GetOutputPort("Start");

            if (!exitPort.IsConnected)
            {
                Debug.LogWarning("Node isn't connected");
                return false;
            }

            IMessageNodes node = exitPort.Connection.node as IMessageNodes;
            node.OnEnter();
            return true;
        }

        public void OnEnter()
        {
            MessageGraph fmGraph = graph as MessageGraph;
            fmGraph.current = this;
        }

        public override object GetValue(NodePort port)
        {
            return null;
        }

        public Message GetMessage()
        {
            throw new System.NotImplementedException();
        }
    }

}
