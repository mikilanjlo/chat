﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace MessagePack
{
    public class CompanionNode : Node
    {
        [Output] public Companion Companion;

        [HideInInspector] public int index = 0;
        [HideInInspector] public string path;
        [HideInInspector] [SerializeField] public Companion CompanionN;

        public override object GetValue(NodePort port)
        {
            return Companion;
        }
    }
}