﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace MessagePack
{
    public interface IMessageNodes
    {
        bool HasMessage { get; }
        bool MoveNext(int variantInd = 0);
        void OnEnter();
        Message GetMessage();
    }
}