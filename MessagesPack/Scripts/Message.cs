﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessagePack
{

    [Serializable]
    public class Message
    {
        [SerializeField] private string _text;
        [SerializeField] private Companion _companion;
        [SerializeField] private List<Variants> _variants /*= new List<Variants>()*/;

        public List<Variants> VariantsList { get => _variants; set => _variants = value; }
        public Companion Companion { get => _companion; set => _companion = value; }
        public string Text { get => _text; set => _text = value; }

        [Serializable]
        public class Variants : EmptyPort
        {
            [HideInInspector] public int Index;
            [HideInInspector] public IMessageNodes messageNode;
            [TextArea] public string Text;
        }
    }
    
}