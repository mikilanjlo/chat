﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNodeEditor;

namespace MessagePack {
    [CustomNodeGraphEditor(typeof(MessageGraph))]
    public class EditorMessageGraph : NodeGraphEditor
    {
        // <summary> 
        /// Overriding GetNodeMenuName lets you control if and how nodes are categorized.
        /// In this example we are sorting out all node types that are not in the XNode.Examples namespace.
        /// </summary>
        public override string GetNodeMenuName(System.Type type)
        {
            if (type.Namespace == "MessagePack")
            {
                return base.GetNodeMenuName(type).Replace("MessagePack/", "");
            }
            else return null;
        }

        public override Color GetTypeColor(Type type)
        {
            if(type != null)
                if (type.IsSubclassOf(typeof(EmptyPort)))
                    type = typeof(EmptyPort);
            return base.GetTypeColor(type);
        }
    }
}