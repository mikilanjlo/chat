﻿using MessagePack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace MessagePackEditor
{
    [CustomNodeEditor(typeof(CompanionNode))]
    public class EditorCompanionNode : NodeEditor
    {

        public override void OnBodyGUI()
        {
            base.OnBodyGUI();
            CompanionNode node = target as CompanionNode;
            MessageGraph graph = node.graph as MessageGraph;

            string[] guids = AssetDatabase.FindAssets("t:Companion");
            Companion[] companions = new Companion[guids.Length];
            int ind = 0;
            for (int i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                Companion companion = AssetDatabase.LoadAssetAtPath(path, typeof(Companion)) as Companion;
                companions[i] = companion;
                if (companion == node.Companion)
                    ind = i;
            }

            ind = EditorGUILayout.Popup(ind, companions.Select(c => c.Name).ToArray());
            node.Companion = companions[ind];
        }
    }
}
