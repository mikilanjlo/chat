﻿using MessagePack;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using XNode;
using XNodeEditor;
using static XNode.Node;

namespace MessagePackEditor
{

    [CustomNodeEditor(typeof(SimpleMessageNode))]
    public class EditorSimpleMessage : NodeEditor
    {
        private GUIStyle _labelGuiStyle;
        private GUIStyle _areaStyle;

        public override void OnCreate()
        {
            _labelGuiStyle = new GUIStyle(GUI.skin.label);
            _labelGuiStyle.fontStyle = FontStyle.Bold;

            _areaStyle = new GUIStyle(GUI.skin.textArea);
            _areaStyle.wordWrap = true;

            base.OnCreate();
        }

        public override void OnBodyGUI()
        {
            serializedObject.Update();
            SimpleMessageNode simpleMessageNode = target as SimpleMessageNode;

            GUILayout.Label("Message", _labelGuiStyle);
            SerializedProperty textSP = serializedObject.FindProperty("text");

            textSP.stringValue = EditorGUILayout.TextArea(textSP.stringValue, _areaStyle);
            //NodeEditorGUILayout.PropertyField(textSP);
            //NodeEditorGUILayout.PropertyField(textSP,true, GUILayout.ExpandHeight(true),GUILayout.Height(1000));

            GUILayout.Space(10f);

            if (simpleMessageNode.Variants.Count == 0)
            {
                NodeEditorGUILayout.PortField(new GUIContent("Companion"), target.GetInputPort("Companion"), GUILayout.MinWidth(0));
                GUILayout.BeginHorizontal();
                NodeEditorGUILayout.PortField(new GUIContent("Enter"), target.GetInputPort("Enter"), GUILayout.MinWidth(0));
                NodeEditorGUILayout.PortField(new GUIContent("Next"), target.GetOutputPort("Next"), GUILayout.MinWidth(0));
                GUILayout.EndHorizontal();
            }
            else
            {
                NodeEditorGUILayout.PortField(new GUIContent("Companion"), target.GetInputPort("Companion"), GUILayout.MinWidth(0));
                NodeEditorGUILayout.PortField(new GUIContent("Enter"), target.GetInputPort("Enter"));
            }

            NodeEditorGUILayout.DynamicPortList("Variants", typeof(Message.Variants), serializedObject, NodePort.IO.Output, Node.ConnectionType.Override );

            serializedObject.ApplyModifiedProperties();

            GUILayout.Space(30f);

            //simpleMessageNode.SetDirty();
        }
    }
}