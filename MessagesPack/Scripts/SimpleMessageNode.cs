﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using XNode;
using System.Linq;

namespace MessagePack
{
    public class SimpleMessageNode : Node, IMessageNodes
    {
        [Input(typeConstraint = TypeConstraint.Inherited)] public EmptyPort Enter;
        [Input(backingValue = ShowBackingValue.Unconnected)] public Companion Companion;
        [Output] public EmptyPort Next;
        [Output(dynamicPortList = true)] public List<Message.Variants> Variants = new List<Message.Variants>();


        [SerializeField] [TextArea] private string text;


        public string Text { get => text; set => text = value; }

        public bool HasMessage
        {
            get
            {
                if (Variants.Count > 0)
                    return true;
                if (Text.Length > 0)
                    return true;
                return false;
            }
        }

        protected override void Init()
        {
            base.Init();
        }

        public override object GetValue(NodePort port)
        {
            return null;
        }


        public bool MoveNext(int variantInd = 0)
        {
            IMessageNodes node;
            if (Variants == null || Variants.Count == 0)
            {

                NodePort exitPort = GetOutputPort("Next");

                if (!exitPort.IsConnected)
                {
                    Debug.LogWarning("Node isn't connected");
                    return false;
                }

                node = exitPort.Connection.node as IMessageNodes;
                node.OnEnter();
                return true;
            }
            else
            {
                NodePort exitPort = GetOutputPort("Variants " + variantInd);

                if (!exitPort.IsConnected)
                {
                    Debug.LogWarning("Node isn't connected");
                    return false;
                }

                node = exitPort.Connection.node as IMessageNodes;
                node.OnEnter();
                return true;
            }
        }

        public void OnEnter()
        {
            MessageGraph fmGraph = graph as MessageGraph;
            fmGraph.current = this;
        }

        public Message GetMessage()
        {
            var variants = new List<Message.Variants>();
            for (int i = 0; i < this.Variants.Count; i++)
            {
                var variant = this.Variants[i];
                variant.Index = i;
                variants.Add(variant);
            }
            var companion = GetInputValue<Companion>("Companion", this.Companion);
            return new Message { Companion = companion, Text = Text, VariantsList = variants };
        }
    }
}
