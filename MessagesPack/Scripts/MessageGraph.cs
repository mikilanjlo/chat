﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XNode;

namespace MessagePack
{
    [Serializable, CreateAssetMenu(fileName = "New Message Chat", menuName = "Message")]
    public class MessageGraph : XNode.NodeGraph
    {

        [Header("Собеседники")]
        [SerializeField] List<Companion> _companions = new List<Companion>();

        public IMessageNodes current;

        public List<Companion> Companions { get => _companions.ToList(); set => _companions = value; }



        public override Node AddNode(Type type)
        {
            if (type == typeof(StartMessage))
            {
                foreach (var node in nodes)
                {
                    if (node is StartMessage)
                        throw new ArgumentException("Start Message already exist");
                }
            }
            if (type == typeof(CompanionNode))
            {
                if (_companions.Count < 0)
                    Debug.LogError("Companions count is 0");
            }
            return base.AddNode(type);
        }
        public override void RemoveNode(Node node)
        {
            current = null;
            base.RemoveNode(node);
        }

        public bool Continue()
        {
            return current.MoveNext();
        }

        public bool Continue(int index)
        {
            return current.MoveNext(index);
        }

       public void ToStart()
        {
            foreach (var node in nodes)
            {
                if(node is StartMessage)
                {
                    current = node as IMessageNodes;
                    break;
                }
            }
        }
    }

    [Serializable]
    public class EmptyPort { }

}