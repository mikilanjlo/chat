﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[Serializable]
public class EndChatEvent : UnityEvent { }
[Serializable]
public class StartChatEvent : UnityEvent { }

namespace MessagePack {
    public class MessageManager : MonoBehaviour
    {
        public StartChatEvent StartChatEvent;
        public EndChatEvent EndChatEvent;
        

        private MessageGraph _curChat;

        private Dictionary<Companion, Action<Message>> Companions = new Dictionary<Companion, Action<Message>>();

        public void Register(Companion companion, Action<Message> action)
        {
            if (Companions.ContainsKey(companion)) throw new ArgumentException("Companion already Exist");

            Companions.Add(companion, action);
        }

        public void StartChat(MessageGraph chat)
        {
            StartChatEvent.Invoke();
            //EventSystem.current.gameObject.SetActive(false);
            Debug.Log("ff");
            _curChat = chat;
            NextMessage();
        }

        public void NextMessage()
        {
            if (!_curChat.Continue())
            {
                EndChat();
                return;
            }

            if (!_curChat.current.HasMessage) return;

            var message = _curChat.current.GetMessage();

            SetMessage(message);
        }

        public void ChooseVariant(int index)
        {
            if (!_curChat.Continue(index)) return;

            if (!_curChat.current.HasMessage) return;

            var message = _curChat.current.GetMessage();

            SetMessage(message);
        }

        protected virtual void SetMessage(Message message)
        {
            Debug.Log(message.Text);
            if (!Companions.ContainsKey(message.Companion)) throw new ArgumentException("Companion is not Finded");

            Companions[message.Companion].Invoke(message);
        }

        private void EndChat()
        {
            //EventSystem.current.gameObject.SetActive(true);
            EndChatEvent.Invoke();
        }

        private void OnApplicationQuit()
        {
            _curChat.ToStart();
        }

    }

}
