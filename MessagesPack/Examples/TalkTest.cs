﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using MessagePack;

public class TalkTest : MonoBehaviour
{
    [SerializeField] private GameObject _talkCloud;
    [SerializeField] private GameObject _textMain;
    [SerializeField] private GameObject _contentVariants;
    [SerializeField] private GameObject _variantsPref;
    [SerializeField] private MessagePack.Companion _companion;
    [SerializeField] private MessageManager _messageManager;

    private TextMeshProUGUI _text;
    private Message _curMessage;
    private bool _isTextPrint;
    private bool _isVariantsSet;
    private IEnumerator _prinTextCorutine;
    // Start is called before the first frame update

    private void Awake()
    {
        _talkCloud.SetActive(false);
    }

    void Start()
    {
        _text = _textMain.GetComponentInChildren<TextMeshProUGUI>();
        _textMain.GetComponent<Button>().onClick.AddListener(NextTextElements);       
        _messageManager.Register(_companion, Say);

        //Type t1 = typeof(System.Object);
        //Type t2 = typeof(String);

        //Debug.Log()
    }

    private void Say(Message message)
    {
        _isVariantsSet = false;
        _talkCloud.SetActive(true);
        _curMessage = message;
        _prinTextCorutine = PrintText();
       StartCoroutine(_prinTextCorutine);
    }


    private IEnumerator PrintText()
    {
        
        var wait = new WaitForSeconds(0.1f);
        
        var text = _curMessage.Text;
        
        if (!String.IsNullOrEmpty(text))
        {
            _isTextPrint = true;
            for (int i = 0; i < text.Length; i++)
            {
                _text.text += text[i];
                yield return wait;
            }
            _isTextPrint = false;
        }
        else
        {
            NextTextElements();
        }

    }

    public void NextTextElements()
    {
        if(_curMessage != null)
            if (_isTextPrint)
            {
                StopCoroutine(_prinTextCorutine);
                _text.text = _curMessage.Text;
                _isTextPrint = false;
            }
            else
            {
                _text.text = "";
                if (_curMessage.VariantsList == null || !(_curMessage.VariantsList.Count > 0))
                {
                    _messageManager.NextMessage();
                    _talkCloud.SetActive(false);
                }
                if (!_isVariantsSet)
                    SetChields(_curMessage.VariantsList);
            }
    }

    private void SetChields(List<Message.Variants> variants)
    {
        _contentVariants.SetActive(true);
        _isVariantsSet = true;
        foreach (var variant in variants)
        {
            var variantGO = Instantiate(_variantsPref, _contentVariants.transform);
            variantGO.GetComponentInChildren<TextMeshProUGUI>().text = variant.Text;
            variantGO.GetComponentInChildren<Button>().onClick.AddListener(() => ChooseVariant(variant.Index));
        }
    }

    private void ClearChields()
    {
        _contentVariants.SetActive(false);
        for (int i = _contentVariants.transform.childCount - 1; i >= 0; i--)
         {
             var chield = _contentVariants.transform.GetChild(i);               
             Destroy(chield.gameObject);
         }
         _isVariantsSet = false;
    }

    public void ChooseVariant(int index)
    {
        ClearChields();
        _messageManager.ChooseVariant(index);
        _talkCloud.SetActive(false);
    }
}
